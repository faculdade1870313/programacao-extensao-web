const express = require('express');
const app = express();
const path = require('path');

// Array para armazenar os alunos
let alunos = [];

// Configurando o Express para servir arquivos estáticos
app.use(express.static(path.join(__dirname, 'public')));

// Middleware para interpretar o corpo da requisição como JSON
app.use(express.json());

// Rota para a página inicial
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Rota para adicionar um aluno
app.post('/adicionar-aluno', (req, res) => {
    // Recuperando os dados do aluno enviados pelo formulário
    const { nome, nota1, nota2 } = req.body;

    // Criando um objeto aluno
    const aluno = {
        nome: nome,
        nota1: nota1,
        nota2: nota2
    };

    // Adicionando o aluno ao array de alunos
    alunos.push(aluno);

    // Calculando a média da turma e a maior média da turma
    const mediaTurma = calcularMediaTurma();
    const maiorMedia = calcularMaiorMedia();

    // Enviando os resultados como resposta
    res.json({ maiorMedia, mediaTurma, alunos });
});

// Função para calcular a média da turma
function calcularMediaTurma() {
    let somaNotas = 0;

    alunos.forEach(aluno => {
        somaNotas += (aluno.nota1 + aluno.nota2);
    });

    return somaNotas / (alunos.length * 2);
}

// Função para calcular a maior média da turma
function calcularMaiorMedia() {
    let maiorMedia = 0;

    alunos.forEach(aluno => {
        const media = (aluno.nota1 + aluno.nota2) / 2;
        if (media > maiorMedia) {
            maiorMedia = media;
        }
    });

    return maiorMedia;
}
// Iniciando o servidor na porta 3000
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Servidor rodando na porta ${PORT}`));