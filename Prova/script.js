// Classe para representar uma transação
class Transacao {
    constructor(descricao, valor, tipo, categoria) {
        this.descricao = descricao;
        this.valor = parseFloat(valor);
        this.tipo = tipo;
        this.categoria = categoria;
    }
}

// Lista de transações
let transacoes = [];

// Lista de categorias
let categorias = [];

// Função para adicionar uma transação
function adicionarTransacao() {
    const descricao = document.getElementById('descricao').value;
    const valor = document.getElementById('valor').value;
    const tipo = document.getElementById('tipo').value;
    const categoria = document.getElementById('categoria').value;

    if (descricao.trim() === '' || isNaN(valor) || valor <= 0 || categoria.trim() === '') {
        alert('Por favor, preencha todos os campos corretamente.');
        return;
    }

    const transacao = new Transacao(descricao, valor, tipo, categoria);
    transacoes.push(transacao);

    document.getElementById('descricao').value = '';
    document.getElementById('valor').value = '';
    document.getElementById('categoria').value = '';

    gerarRelatorio();
}

// Função para adicionar uma categoria
function adicionarCategoria() {
    const categoria = document.getElementById('categoria').value;

    if (categoria.trim() === '') {
        alert('Por favor, insira um nome de categoria válido.');
        return;
    }

    categorias.push(categoria);

    document.getElementById('categoria').value = '';

    gerarRelatorio();
}

// Função para gerar o relatório de gastos
function gerarRelatorio() {
    const listaRelatorio = document.getElementById('listaRelatorio');
    listaRelatorio.innerHTML = '';

    categorias.forEach(categoria => {
        const total = transacoes
            .filter(transacao => transacao.categoria === categoria)
            .reduce((acc, transacao) => {
                return transacao.tipo === 'despesa' ? acc - transacao.valor : acc + transacao.valor;
            }, 0);

        const itemLista = document.createElement('div');
        itemLista.classList.add('item-categoria');
        itemLista.innerHTML = `<strong>${categoria}</strong>: R$ ${total.toFixed(2)}`;
        listaRelatorio.appendChild(itemLista);
    });
}
