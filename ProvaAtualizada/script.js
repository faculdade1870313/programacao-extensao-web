
let categorias = [];


let transacoes = [];


document.getElementById('btnAdicionarCategoria').addEventListener('click', adicionarCategoria);

function adicionarCategoria() {
    const nomeCategoria = document.getElementById('nomeCategoria').value.trim();

    if (nomeCategoria === '') {
        console.error('Por favor, insira um nome de categoria válido.');
        return;
    }

    
    if (categorias.includes(nomeCategoria)) {
        console.error('Esta categoria já existe.');
        return;
    }

    categorias.push(nomeCategoria);
    console.log('Categoria adicionada com sucesso!');

    document.getElementById('nomeCategoria').value = '';

   
    preencherSelectCategorias();
}

document.getElementById('btnAdicionarTransacao').addEventListener('click', adicionarTransacao);

function adicionarTransacao() {
    const descricao = document.getElementById('descricao').value;
    const valor = document.getElementById('valor').value;
    const categoria = document.getElementById('categoria').value;
    const tipo = document.getElementById('tipo').value;

    if (descricao.trim() === '' || isNaN(valor) || valor <= 0 || categoria.trim() === '') {
        console.error('Por favor, preencha todos os campos corretamente.');
        return;
    }

    const transacao = {
        descricao,
        valor: parseFloat(valor),
        categoria,
        tipo
    };

    transacoes.push(transacao);
    console.log('Transação adicionada com sucesso!');

    document.getElementById('descricao').value = '';
    document.getElementById('valor').value = '';

    gerarRelatorio();
}


function preencherSelectCategorias() {
    const selectCategoria = document.getElementById('categoria');

    selectCategoria.innerHTML = '<option value="">Selecione a Categoria</option>';

    categorias.forEach(categoria => {
        const option = document.createElement('option');
        option.value = categoria;
        option.textContent = categoria;
        selectCategoria.appendChild(option);
    });
}

function gerarRelatorio() {
    const listaRelatorio = document.getElementById('listaRelatorio');
    listaRelatorio.innerHTML = '';

    const categoriasRelatorio = [...new Set(transacoes.map(transacao => transacao.categoria))]; 

    categoriasRelatorio.forEach(categoria => {
        const total = transacoes
            .filter(transacao => transacao.categoria === categoria)
            .reduce((acc, transacao) => {
                return transacao.tipo === 'despesa' ? acc - transacao.valor : acc + transacao.valor;
            }, 0);

        const itemLista = document.createElement('div');
        itemLista.classList.add('item-categoria');
        itemLista.innerHTML = `<strong>${categoria}</strong>: R$ ${total.toFixed(2)}`;
        listaRelatorio.appendChild(itemLista);
    });
}
