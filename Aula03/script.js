class RastreadorVendas {
    constructor() {
        this.vendas = [];
    }

    adicionarVenda(valor) {
        this.vendas.push(valor);
    }

    obterMaiorVenda() {
        return Math.max(...this.vendas);
    }

    obterMediaVendas() {
        if (this.vendas.length === 0) return 0;
        const total = this.vendas.reduce((acc, curr) => acc + curr, 0);
        return total / this.vendas.length;
    }

    gerarListaVendasHTML() {
        let html = '<h2>Lista de Vendas:</h2><ul>';
        this.vendas.forEach(venda => {
            html += `<li>${venda}</li>`;
        });
        html += '</ul>';
        return html;
    }
}

const rastreadorVendas = new RastreadorVendas();

function adicionarVenda() {
    const inputVenda = document.getElementById('inputVenda');
    const valorVenda = parseFloat(inputVenda.value);
    if (!isNaN(valorVenda)) {
        rastreadorVendas.adicionarVenda(valorVenda);
        atualizarResultado();
        inputVenda.value = '';
    } else {
        alert('Por favor, insira um valor válido.');
    }
}

function atualizarResultado() {
    const maiorVendaSpan = document.getElementById('maiorVenda');
    maiorVendaSpan.textContent = rastreadorVendas.obterMaiorVenda();

    const mediaVendasSpan = document.getElementById('mediaVendas');
    mediaVendasSpan.textContent = rastreadorVendas.obterMediaVendas().toFixed(2);
}

function gerarHTML() {
    const listaVendasDiv = document.getElementById('listaVendas');
    listaVendasDiv.innerHTML = rastreadorVendas.gerarListaVendasHTML();
}
